# TicketSys

A Ticket System for real estate maintenance requests.

Use Case: Tenants submit maintenance ticket through tenant entry and works contact the tenants based on the information the tenants provided in the ticket.

This application contains three pages:

- Tenant page
- Worker page
- Administrator page