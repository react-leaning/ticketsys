import React from 'react';
import { Layout, Menu, Typography } from 'antd';
import axios from "axios";
import Listing from './Listing';
import ContactForm from './ContactForm';
import Home from './Home';
import './App.css';
import 'antd/dist/antd.css';
const { Header, Content, Footer } = Layout;
const { Text } = Typography;

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentKey: '1',
      addrList: [],
    };
    this.handleClick = this.handleClick.bind(this);
    this.getAddrList = this.getAddrList.bind(this);
    this.viewAvailability = this.viewAvailability.bind(this);
    this.viewHome = this.viewHome.bind(this);
  }

  componentDidMount() {
    this.getAddrList();
  }


  viewAvailability() {
    this.setState({
      currentKey: '2'
    });
  }

  viewHome() {
    this.setState({
      currentKey: '1'
    });
  }

  getAddrList() {
    let self = this;
    axios
      .get("/api/property?attributes=address_short")
      .then(function (response) {
        console.log(response.data);
        self.setState(
          {
            addrList: response.data.result
          }
        );
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  handleClick({ key }) {
    this.setState({
      currentKey: key
    });
  }

  renderContent(key) {
    switch (key) {
      case '1':
        return <Home viewAvailability={this.viewAvailability} />;
      case '2':
        return <Listing addrList={this.state.addrList} />;
      case '3':
        return <ContactForm addrList={this.state.addrList} />;
    }
  }

  render() {

    return (
      <Layout>
        <Header className="header">
          <a href='#' onClick={this.viewHome}><font color='#fff' size='+1'>CIRCLE NORTH PROPERTIES</font></a>
          <Menu
            theme="dark"
            mode="horizontal"
            selectedKeys={[this.state.currentKey]}
            style={{ lineHeight: '64px', float: 'right' }}
            onClick={this.handleClick}
          >
            <Menu.Item key="1" >HOME</Menu.Item>
            <Menu.Item key="2" >RENTALS</Menu.Item>
            <Menu.Item key="3" >CONTACT</Menu.Item>
          </Menu>
        </Header>
        <Content style={{ padding: '50px 50px' }}>
          {this.renderContent(this.state.currentKey)}
        </Content>
        <Footer style={{ textAlign: 'center', background: '#001529' }}><Text style={{ color: 'white' }}>CIRCLE NORTH PROPERTIES</Text></Footer>
      </Layout>
    );
  }
}
export default App;
