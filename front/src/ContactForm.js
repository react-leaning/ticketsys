import React from "react";
import axios from "axios";
import { Form, Select, Input, Button, Descriptions, Typography, Result } from "antd";
import moment from 'moment'
import "./ContactForm.css";
const { Option } = Select;
const { Title } = Typography;
// const server = "localhost";

class ContactForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      contactType: 0, // 0: maintenance request, 1: inquery
      submitted: false, // if the form is submitted
      submit_response: null
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleAddressChange = this.handleAddressChange.bind(this);
    this.handleTypeChange = this.handleTypeChange.bind(this);
    this.returnAddrOptions = this.returnAddrOptions.bind(this);
  }

  handleAddressChange(value) {
    console.log(value);
  }

  handleTypeChange(value) {
    console.log(value);
    this.setState({
      contactType: value,
    });

    console.log(this.state.contactType);
  }

  handleSubmit(e) {
    e.preventDefault();
    const self = this;
    let url = this.state.contactType === 0 ? "api/claim" : "api/inquiry";
    this.props.form.validateFields((err, values) => {
      if (!err) {
        axios
          .post(url, values)
          .then(function (response) {
            console.log(response.data);
            self.setState({
              submitted: true,
              submit_response: response.data.result
            });
          })
          .catch(function (error) {
            console.log(error);
          });
      } else {
        console.log("form not submitted.");
        console.log(values);
      }
    });
  }

  returnAddrOptions() {
    const options = [];

    //render a list of menyItems based on how many addresses got from database
    for (const [index, value] of this.props.addrList.entries()) {
      options.push(<Option value={value.address_short}>{value.address_short}</Option>);
    }
    return options;
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 5 },
        md: { span: 5 },
        lg: { span: 5 },
        xl: { span: 5 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 14 },
        md: { span: 14 },
        lg: { span: 14 },
        xl: { span: 14 }
      }
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 10
        },
        sm: {
          span: 24,
          offset: 15
        },
        md: {
          span: 24,
          offset: 15
        },
        lg: {
          span: 24,
          offset: 11
        },
        xl: {
          span: 24,
          offset: 11
        }
      }
    };

    return (
      this.state.submitted ?
        this.state.contactType === 0 ?
          <>
            <Result
              status="success"
              title="We received your request. Here's detail:"
            />
            <Descriptions column={4}>
              <Descriptions.Item label="Ticket Number">{this.state.submit_response.ticket_num}</Descriptions.Item>
              <Descriptions.Item label="Address">
                {this.state.submit_response.address_short} <br />
                Room {this.state.submit_response.room_num}
              </Descriptions.Item>
              <Descriptions.Item label="Date">{moment(this.state.submit_response.createdAt).format('YYYY/MM/DD')}</Descriptions.Item>
              <Descriptions.Item label="Your Info">
                Name: {this.state.submit_response.name} <br />
                Phone Number: {this.state.submit_response.phone_num} <br />
                Email: {this.state.submit_response.email}
              </Descriptions.Item>
              <Descriptions.Item label="Subject" span={3}>{this.state.submit_response.subject}</Descriptions.Item>
              <Descriptions.Item label="Message">{this.state.submit_response.body}</Descriptions.Item>
            </Descriptions>
          </> :
          <>
            <Result
              status="success"
              title="We received your inquiry. Here's detail:"
            />
            <Descriptions column={4}>
              <Descriptions.Item label="Address">
                {this.state.submit_response.address_short} <br />
                Room {this.state.submit_response.room_num}
              </Descriptions.Item>
              <Descriptions.Item label="Date">{moment(this.state.submit_response.createdAt).format('YYYY/MM/DD')}</Descriptions.Item>
              <Descriptions.Item label="Your Info">
                Name: {this.state.submit_response.name} <br />
                Phone Number: {this.state.submit_response.phone_num} <br />
                Email: {this.state.submit_response.email}
              </Descriptions.Item>
              <Descriptions.Item label="Message">{this.state.submit_response.body}</Descriptions.Item>
            </Descriptions>
          </>
        :
        <Form onSubmit={this.handleSubmit}>
          <Form.Item {...formItemLayout} label="Contact type">
            {getFieldDecorator("contactType", {
              initialValue: this.state.contactType,
              rules: [
                {
                  required: true,
                  message: "Please Select an contact type."
                }
              ]
            })(
              <Select onChange={this.handleTypeChange}>
                <Option value={0}>Maintenance request</Option>
                <Option value={1}>Inquiry</Option>
              </Select>
            )}
          </Form.Item>

          <Form.Item {...formItemLayout} label="Adddress">
            {getFieldDecorator("address_short", {
              initialValue: this.props.addrList[0].address_short,
              rules: [
                {
                  required: true,
                  message: "Please Select an address."
                }
              ]
            })(
              <Select onChange={this.handleAddressChange}>
                {this.returnAddrOptions()}
              </Select>
            )}
          </Form.Item>
          {
            this.state.contactType === 0 ?
              <Form.Item {...formItemLayout} label="Apartment number:">
                {getFieldDecorator("room_num", {
                  initialValue: "",
                  rules: [
                    {
                      required: false
                    }
                  ]
                })(<Input />)}
              </Form.Item> : null
          }
          <Form.Item {...formItemLayout} label="Name:">
            {getFieldDecorator("name", {
              rules: [
                {
                  required: true
                }
              ]
            })(<Input />)}
          </Form.Item>
          <Form.Item {...formItemLayout} label="E-mail">
            {getFieldDecorator("email", {
              rules: [
                {
                  type: "email",
                  message: "The input is not valid E-mail."
                },
                {
                  required: true
                }
              ]
            })(<Input />)}
          </Form.Item>
          <Form.Item {...formItemLayout} label="Phone number">
            {getFieldDecorator("phone_num", {
              rules: [
                {
                  required: true
                }
              ]
            })(<Input />)}
          </Form.Item>
          {
            this.state.contactType === 0 ?
              <Form.Item {...formItemLayout} label="Subject">
                {getFieldDecorator("subject", {
                  rules: [
                    {
                      required: true
                    }
                  ]
                })(<Input />)}
              </Form.Item> : null
          }
          <Form.Item {...formItemLayout} label="message">
            {getFieldDecorator("body", {
              initialValue: "",
              rules: [
                {
                  required: false
                }
              ]
            })(<Input.TextArea autosize={{ minRows: 3, maxRows: 8 }} />)}
          </Form.Item>
          <Form.Item {...tailFormItemLayout}>
            <Button type="primary" htmlType="submit">
              Submit
          </Button>
          </Form.Item>
        </Form >
    );
  }
}
export default Form.create()(ContactForm);
