import React from "react";
import { Spin, Typography, Card } from 'antd';
import Gallery from 'react-grid-gallery';
const { Title, Paragraph } = Typography;
class Details extends React.Component {
    constructor(props) {
        super(props);
        this.renderImageList = this.renderImageList.bind(this);
    }

    renderImageList() {
        if (this.props.images) {
            let image_list = [];
            this.props.images.forEach(element => {
                image_list.push({
                    alt: element.name,
                    src: element.file_path,
                    thumbnail: element.file_path
                });
            });
            return <Gallery
                images={image_list}
                enableImageSelection={false} />;
        } else {
            return <Spin size="large" />
        }
    }

    render() {
        if (this.props.details) {
            return (
                <>
                    <Title type="primary">Rental price starting at ${this.props.details.price}/mo</Title>
                    <Paragraph ellipsis={{ rows: 3, expandable: true }}>Located at: <a target="_blank" href={encodeURI('https://www.google.com/maps/search/?api=1&query=' + this.props.details.address_long)}>{this.props.details.address_long}</a></Paragraph>
                    <Paragraph style={{ fontSize: '20px' }} ellipsis={{ rows: 3, expandable: true }}>{this.props.details.availability_detail}</Paragraph>
                    <Paragraph style={{ fontSize: '20px' }} ellipsis={{ rows: 3, expandable: true }}>{this.props.details.info}</Paragraph>
                    <Paragraph style={{ fontSize: '20px' }} ellipsis={{ rows: 3, expandable: true }}>{this.props.details.feature}</Paragraph>
                    {this.renderImageList()}
                </>
            );
        } else {
            return (
                <Spin size="large" />
            );
        }
    }
}

export default Details;