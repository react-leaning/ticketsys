import React from "react";
import { Typography } from 'antd';
import './Home.css';
import universitycirclePic from './universitycircle-wadelagoon-crediterikdrost_flickrcc.jpg';
import neighborhoodPic from './2020_815009999_PH_11_MESWDJGZYLYE2.jpg';
const { Title, Text } = Typography;

class Home extends React.Component {
    render() {
        return (
            <>
                <div class="container">
                    <img src={universitycirclePic} alt="home" style={{ width: '100%' }} id='home_img' />
                    <div class="image_center_text">
                        <Title style={{ color: 'white' }}>Affordable and convenient.<br />Just a short ride downtown.</Title>
                        <a href='#' onClick={this.props.viewAvailability}><Title level={3} style={{ color: 'white' }}>View Availabilities</Title></a>
                    </div>
                </div>
                <div class="container" style={{ margin: '7em', fontSize: '20px' }}>
                    <Title level={2}>Making your everyday, easy.</Title>
                    <Text>Located near University Circle, Circle North Properties offers convenience and affordability. All</Text>
                    <br />
                    <Text>within walking distance to hospitals, universities, restaurants, museams and the transit system.</Text>
                </div>

                <div class="container">
                    <img src={neighborhoodPic} alt="home" style={{ width: '100%' }} id='home_img' />
                    <div class="image_center_text">
                        <Title style={{ color: 'white' }}>Neighborhood</Title>
                    </div>
                </div>

                <div class="container" style={{ margin: '7em', fontSize: '20px' }}>
                    <Title level={2}>Living it up in Circle North.</Title>
                    <Text>At Circle North Properties, we believe in convenience at an affordable price.</Text>
                </div>
            </>

        );
    }
}


export default Home;