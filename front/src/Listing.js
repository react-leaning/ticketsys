import React from "react";
import { Layout, Menu, Tooltip } from 'antd';
import Details from './Details'
import axios from "axios";
import 'antd/dist/antd.css';

const { Content, Sider } = Layout;
class Listing extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentAddrIndex: 0,
            currentDetails: null,
            currentImages: null
        };
        this.handleClick = this.handleClick.bind(this);
        this.setCurrentAddrDetailsAndImgs = this.setCurrentAddrDetailsAndImgs.bind(this);
    }

    componentDidMount() {
        this.setCurrentAddrDetailsAndImgs(this.props.addrList[this.state.currentAddrIndex].address_short);
    }

    setCurrentAddrDetailsAndImgs(addr) {
        let self = this;
        axios
            .get(`/api/property/${addr}`)
            .then(function (response) {
                console.log(response.data);
                self.setState(
                    {
                        currentDetails: response.data.result
                    }
                );
            })
            .catch(function (error) {
                console.log(error);
            });

        axios
            .get(`/api/property/${addr}/image`)
            .then(function (response) {
                console.log(response.data);
                self.setState(
                    {
                        currentImages: response.data.result
                    }
                );
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    handleClick({ key }) {
        this.setState({
            currentAddrIndex: key,
        });
        this.setCurrentAddrDetailsAndImgs(this.props.addrList[key].address_short);

    }

    render() {
        const menuItems = [];

        //render a list of menyItems based on how many addresses got from database
        for (const [index, value] of this.props.addrList.entries()) {
            menuItems.push(<Menu.Item key={index}><Tooltip title={value.address_short}>{value.address_short}</Tooltip></Menu.Item>)
        }

        return (
            <Layout style={{ padding: '24px 0', background: '#fff' }}>
                <Sider width={200} style={{ background: '#fff' }}>
                    <Menu
                        mode="inline"
                        defaultSelectedKeys={['0']}
                        style={{ height: '100%' }}
                        onClick={this.handleClick}
                    >
                        {menuItems}
                    </Menu>
                </Sider>
                <Content style={{ padding: '0 24px', minHeight: 280 }}>
                    <Details details={this.state.currentDetails} images={this.state.currentImages}></Details>
                </Content>
            </Layout>
        );
    }
}
export default Listing;
