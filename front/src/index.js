import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
// ReactDOM.render(
//     <Row>
//         <Col xs={2} sm={2} md={2} lg={2} xl={6}>
//         </Col>
//         <Col xs={20} sm={20} md={20} lg={20} xl={12}>
//             <App />
//         </Col>
//         <Col xs={2} sm={2} md={2} lg={2} xl={6}>
//         </Col>
//     </Row>,
//     document.getElementById('root')
// );

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
