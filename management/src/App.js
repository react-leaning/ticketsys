import React from 'react';
import { Layout, Menu, Typography } from 'antd';
import TicketBox from './TicketBox';
import InquiryBox from './InquiryBox';
import Management from './Management';
import './App.css';
import 'antd/dist/antd.css';
const { Header, Content, Footer } = Layout;
const { Text } = Typography;



class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentKey: '1',
    };
    this.handleClick = this.handleClick.bind(this);
    this.viewHome = this.viewHome.bind(this);
  }

  viewHome() {
    this.setState({
      currentKey: '1'
    });
  }

  handleClick({ key }) {
    this.setState({
      currentKey: key
    });
  }

  renderContent(key) {
    switch (key) {
      case '1':
        return <TicketBox />;
      case '2':
        return <InquiryBox />;
      case '3':
        return <Management />;
    }
  }

  render() {
    return (
      <Layout>
        <Header className="header">
          <a href='#' onClick={this.viewHome}><font color='#fff' size='+1'>CIRCLE NORTH PROPERTIES</font></a>
          <Menu
            theme="dark"
            mode="horizontal"
            selectedKeys={[this.state.currentKey]}
            style={{ lineHeight: '64px', float: 'right' }}
            onClick={this.handleClick}
          >
            <Menu.Item key="1">TICKETS</Menu.Item>
            <Menu.Item key="2">INQUIRIES</Menu.Item>
            <Menu.Item key="3">MANAGEMENT</Menu.Item>
          </Menu>
        </Header>
        <Content style={{ padding: '50px 50px' }}>
          {this.renderContent(this.state.currentKey)}
        </Content>
        <Footer style={{ textAlign: 'center', background: '#001529' }}><Text style={{ color: 'white' }}>CIRCLE NORTH PROPERTIES</Text></Footer>
      </Layout>
    );
  }
}
export default App;
