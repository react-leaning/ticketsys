import React from "react";
import axios from "axios";
import { Form, Input, Button, InputNumber } from "antd";

class CreateAddressForm extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleSubmit(e) {
    e.preventDefault();
    const self = this;
    let url = "api/property";
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.setModalConfirmLoading(true);
        axios
          .post(url, values)
          .then(function (response) {
            console.log(response.data);
            self.props.handleModalOk();
          })
          .catch(function (error) {
            console.log(error);
          });
      } else {
        console.log("form not submitted.");
        console.log(values);
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <Form onSubmit={this.handleSubmit}>
        <Form.Item label="Apartment/House Name:">
          {getFieldDecorator("address_short", {
            rules: [
              {
                required: true,
                message: 'Apartment/House Name is required'
              }
            ]
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Full Address:">
          {getFieldDecorator("address_long", {
            rules: [
              {
                required: true,
                message: 'Full Address is required'
              }
            ]
          })(<Input />)}
        </Form.Item>
        <Form.Item label="Price:">
          {getFieldDecorator("price", {
            initialValue: 1000,
            rules: [
              {
                required: true,
                message: 'price is required'
              }
            ]
          })(<InputNumber />)}
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">
            Create
          </Button>
        </Form.Item>
      </Form >
    );
  }
}
export default Form.create()(CreateAddressForm);
