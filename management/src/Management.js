import React from "react";
import { Layout, Menu, Spin, Typography, Divider, InputNumber, Select, Tooltip, Button, Modal, Form, Input } from 'antd';
import axios from 'axios';
import CreateAddressForm from './CreateAddressForm';
import ImageBox from './ImageBox';
const { Title, Text, Paragraph } = Typography;
const { Content, Sider } = Layout;
const { Option } = Select;

//this compoment reprents a list of tickets, and have some basic filter
class Management extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            addrList: [],
            currentAddrIndex: 0,
            currentDetails: null,
            mailing_list: [],
            modalVisible: false,
            modalConfirmLoading: false,
            fileList: null
        };

        this.getMailingList = this.getMailingList.bind(this);
        this.updateMailingList = this.updateMailingList.bind(this);
        this.mailingListToParagraph = this.mailingListToParagraph.bind(this);
        this.getAddrList = this.getAddrList.bind(this);
        this.renderMenuItems = this.renderMenuItems.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.setCurrentAddrDetailsAndImgs = this.setCurrentAddrDetailsAndImgs.bind(this);
        this.onPriceChange = this.onPriceChange.bind(this);
        this.onAvailabilityChange = this.onAvailabilityChange.bind(this);
        this.onAvailabilityDetailChange = this.onAvailabilityDetailChange.bind(this);
        this.onInfoChange = this.onInfoChange.bind(this);
        this.onFeatureChange = this.onFeatureChange.bind(this);
        this.handleModalOk = this.handleModalOk.bind(this);
        this.handleModalCancel = this.handleModalCancel.bind(this);
        this.setModalConfirmLoading = this.setModalConfirmLoading.bind(this);
        this.setFileList = this.setFileList.bind(this);
    }

    handleModalCancel() {
        this.setState({
            modalVisible: false
        });
    }

    handleModalOk() {
        this.setState({
            modalVisible: false,
            modalConfirmLoading: false
        });
        this.getAddrList();
    }

    setModalConfirmLoading(i) {
        this.setState({
            modalConfirmLoading: i
        });
    }

    onPriceChange(price) {
        console.log('price is ' + price);
        let self = this;
        let values = {
            'price': price
        };
        axios
            .post(`api/property/${this.state.currentDetails.address_short}`, values)
            .then(function (response) {
                console.log(response.data);
                let currentDetails = { ...self.state.currentDetails }
                currentDetails.price = price;
                self.setState({
                    currentDetails: currentDetails
                });
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    onAvailabilityChange(availability) {
        console.log('availability is ' + availability);
        let self = this;
        let values = {
            'available': availability
        };
        axios
            .post(`api/property/${this.state.currentDetails.address_short}`, values)
            .then(function (response) {
                console.log(response.data);
                let currentDetails = { ...self.state.currentDetails }
                currentDetails.available = availability;
                self.setState({
                    currentDetails: currentDetails
                });
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    onAvailabilityDetailChange(availability_detail) {
        console.log('availability detail is ' + availability_detail);
        let self = this;
        let values = {
            'availability_detail': availability_detail
        };
        axios
            .post(`api/property/${this.state.currentDetails.address_short}`, values)
            .then(function (response) {
                console.log(response.data);
                let currentDetails = { ...self.state.currentDetails }
                currentDetails.availability_detail = availability_detail;
                self.setState({
                    currentDetails: currentDetails
                });
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    onInfoChange(info) {
        console.log('info is ' + info);
        let self = this;
        let values = {
            'info': info
        };
        axios
            .post(`api/property/${this.state.currentDetails.address_short}`, values)
            .then(function (response) {
                console.log(response.data);
                let currentDetails = { ...self.state.currentDetails }
                currentDetails.info = info;
                self.setState({
                    currentDetails: currentDetails
                });
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    onFeatureChange(feature) {
        console.log('feature is ' + feature);
        let self = this;
        let values = {
            'feature': feature
        };
        axios
            .post(`api/property/${this.state.currentDetails.address_short}`, values)
            .then(function (response) {
                console.log(response.data);
                let currentDetails = { ...self.state.currentDetails }
                currentDetails.feature = feature;
                self.setState({
                    currentDetails: currentDetails
                });
            })
            .catch(function (error) {
                console.log(error);
            });
    }


    getAddrList() {
        let self = this;
        axios
            .get("/api/property?attributes=address_short")
            .then(function (response) {
                console.log(response.data);
                self.setState(
                    {
                        addrList: response.data.result
                    }
                );
                self.setCurrentAddrDetailsAndImgs(response.data.result[0].address_short);
            })
            .catch(function (error) {
                console.log(error);
            });
    }


    getMailingList() {
        const self = this;
        axios
            .get(`api/mailinglist`)
            .then(function (response) {
                console.log(response.data);
                self.setState(
                    {
                        mailing_list: response.data.result
                    }
                );
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    mailingListToParagraph() {
        let paragraph = '';
        this.state.mailing_list.forEach(element => {
            paragraph += element.name;
            paragraph += ', '
            paragraph += element.email;
            paragraph += '; '
        });
        return paragraph;
    }

    updateMailingList(paragraph) {
        console.log('update mailing list:');
        console.log(paragraph);
        let new_mailingList = [];
        let list = paragraph.trim().split(';');
        list.forEach(element => {
            element = element.trim();
            if (element !== '') {
                let pair = element.split(',');
                let new_email = {
                    'name': pair[0].trim(),
                    'email': pair[1].trim()
                };
                new_mailingList.push(new_email);
            }
        });
        console.log("new mailingList: ");
        console.log(new_mailingList);

        this.setState({
            mailing_list: new_mailingList
        });


        axios
            .post('api/mailinglist', new_mailingList)
            .then(function (response) {
                console.log(response.data);
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    renderMenuItems() {
        if (this.state.addrList) {
            const menuItems = [];
            //render a list of menyItems based on how many addresses got from database
            for (const [index, value] of this.state.addrList.entries()) {
                menuItems.push(<Menu.Item key={index}><Tooltip title={value.address_short}>{value.address_short}</Tooltip></Menu.Item>)
            }
            let addNewAddress = <Menu.Item key={this.state.addrList.length}><Tooltip title='Add a new address'><Button>Add New Address</Button></Tooltip></Menu.Item>
            menuItems.push(addNewAddress);
            return menuItems;
        } else {
            return <Spin size="large" />
        }
    }

    setCurrentAddrDetailsAndImgs(addr) {
        let self = this;
        axios
            .get(`/api/property/${addr}`)
            .then(function (response) {
                console.log(response.data);
                self.setState(
                    {
                        currentDetails: response.data.result
                    }
                );
            })
            .catch(function (error) {
                console.log(error);
            });

        axios
            .get(`/api/property/${addr}/image`)
            .then(function (response) {
                console.log(response.data);
                self.setState(
                    {
                        fileList: response.data.result.map(x => ({
                            ...x,
                            uid: x.id,
                            status: 'done',
                            url: x.file_path
                        }))
                    }
                );
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    setFileList(fileList){
        this.setState({ fileList });
    };

    handleClick({ key }) {
        if (key < this.state.addrList.length) {
            this.setState({
                currentAddrIndex: key,
            });
            this.setCurrentAddrDetailsAndImgs(this.state.addrList[key].address_short);
        } else {
            this.setState({
                modalVisible: true
            });
        }

    }

    componentDidMount() {
        this.getMailingList();
        this.getAddrList();
    }

    render() {
        return (
            <>
                <Title level={4}>Mailing List</Title>
                <br />
                <Text>You can edit mailing list here, the mailing list here has to follow the following syntax:</Text>
                <br />
                <Text strong>Name1, email1; Name2, email2; Name3, email3; ...</Text>
                <br />

                {
                    this.mailing_list ?
                        <Spin size="small" /> :
                        <Paragraph editable={{ onChange: this.updateMailingList }}>{this.mailingListToParagraph()}</Paragraph>
                }
                < Divider />
                <Title level={4}>Housing Details</Title>
                <Layout style={{ padding: '24px 0', background: '#fff' }}>
                    <Sider width={200} style={{ background: '#fff' }}>
                        <Menu
                            mode="inline"
                            defaultSelectedKeys={['0']}
                            style={{ height: '100%' }}
                            onClick={this.handleClick}
                        >
                            {this.renderMenuItems()}
                        </Menu>
                    </Sider>
                    <Content style={{ padding: '0 24px', minHeight: 280 }}>
                        {this.state.currentDetails ?
                            <>
                                <Text strong>Price: $</Text><InputNumber min={0} value={this.state.currentDetails.price} onChange={this.onPriceChange} />
                                <br />
                                <br />
                                <Text strong>Availability:&nbsp;&nbsp;</Text>
                                <Select value={this.state.currentDetails.available} maxTagTextLength={15} onChange={this.onAvailabilityChange}>
                                    <Option value={true}>Available</Option>
                                    <Option value={false}>Not Available</Option>
                                </Select>
                                <br />
                                <br />
                                <Text strong>availability_detail:</Text>
                                <br />
                                <Paragraph editable={{ onChange: this.onAvailabilityDetailChange }} >{this.state.currentDetails.availability_detail}</Paragraph>
                                <Divider />
                                <Text strong>Info:</Text>
                                <Paragraph editable={{ onChange: this.onInfoChange }} >{this.state.currentDetails.info}</Paragraph>
                                <Divider />
                                <Text strong>Feature:</Text>
                                <Paragraph editable={{ onChange: this.onFeatureChange }} >{this.state.currentDetails.feature}</Paragraph>
                                <Divider />
                                <Text strong>Images:</Text>
                                <br />
                                <br />
                                <ImageBox address_short={this.state.currentDetails.address_short} fileList={this.state.fileList} setFileList={this.setFileList} />
                                <Divider />
                                <Text strong>Delete Address:</Text>
                                <br />
                                <br />
                                <Button type="danger">Delete This Address</Button>
                            </> :
                            <Spin size='large' />
                        }
                    </Content>
                </Layout>

                <Modal
                    title="Add New Address"
                    visible={this.state.modalVisible}
                    confirmLoading={this.state.modalConfirmLoading}
                    onCancel={this.handleModalCancel}
                    footer={null}
                >
                    <CreateAddressForm handleModalOk={this.handleModalOk} setModalConfirmLoading={this.setModalConfirmLoading} />
                </Modal>

            </>
        );
    }
}

export default Management;