import React from "react";
import { Descriptions, Badge, Popover, Button } from 'antd';
import moment from 'moment';
import axios from 'axios';
//this compoment reprents one ticket
class Ticket extends React.Component {
    constructor(props) {
        super(props);
        
        this.flipResolvedStatus = this.flipResolvedStatus.bind(this);
    }

    
    flipResolvedStatus() {
        const self = this;
        const values = {
            resolved: !this.props.info.resolved
        };
        let url = `api/claim/${this.props.info.ticket_num}`;
        axios
            .post(url, values)
            .then(function (response) {
                console.log(response.data);
                self.props.search();
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    render() {
        const popoverContent = (
            <div>
                <Button type="primary" onClick={this.flipResolvedStatus}>
                    {this.props.info.resolved ? 'Set ticket Unresolved' : 'Set ticket Resolved'}
                </Button>
            </div>
        );
        return (
            <Descriptions bordered column={4}>
                <Descriptions.Item label="Ticket Number">{this.props.info.ticket_num}</Descriptions.Item>
                <Descriptions.Item label="Address">
                    {this.props.info.address_short} <br />
                    Room {this.props.info.room_num}
                </Descriptions.Item>
                <Descriptions.Item label="Claim Date">{moment(this.props.info.createdAt).format('YYYY/MM/DD')}</Descriptions.Item>
                <Descriptions.Item label="Status">
                    <Popover content={popoverContent} trigger="hover">
                        {this.props.info.resolved ?
                            <Badge status="success" text={'Resolved at ' + moment(this.props.info.updatedAt).format('YYYY/MM/DD')} /> :
                            <Badge status="processing" text="Unresovled" />
                        }
                    </Popover>
                </Descriptions.Item>
                <Descriptions.Item label="Tenent Info">
                    Name: {this.props.info.name} <br />
                    Phone Number: {this.props.info.phone_num} <br />
                    Email: {this.props.info.email}
                </Descriptions.Item>
                <Descriptions.Item label="Subject" span={3}>{this.props.info.subject}</Descriptions.Item>
                <Descriptions.Item label="Message">{this.props.info.body}</Descriptions.Item>
            </Descriptions>
        );
    }
}

export default Ticket;