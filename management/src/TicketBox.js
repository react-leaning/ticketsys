import React from "react";
import { Form, DatePicker, Checkbox, Button, List, Badge } from 'antd';
import moment from 'moment';
import axios from 'axios'
import Ticket from "./Ticket";
const { RangePicker } = DatePicker;

//this compoment reprents a list of tickets, and have some basic filter
class TicketBox extends React.Component {
    constructor(props) {
        super(props);
        var twoWeeksAgo = new Date();
        twoWeeksAgo.setDate(twoWeeksAgo.getDate() - 14);
        this.state = {
            showUnresolved: true,
            showResolved: false,
            startDate: moment().subtract(30, 'days'),
            endDate: moment(),
            searchResults: []
        };
        this.handleSearch = this.handleSearch.bind(this);
        this.search = this.search.bind(this);
        this.onRangeChange = this.onRangeChange.bind(this);
        this.onUnresolvedChange = this.onUnresolvedChange.bind(this);
        this.onResolvedChange = this.onResolvedChange.bind(this);
        this.isChecked = this.isChecked.bind(this);
    }

    componentDidMount() {
        this.search();
        setInterval(this.search, 60000);
    }

    search() {
        const self = this;
        if (!this.state.startDate || !this.state.endDate) {
            console.log("Range not set, can't search");
            return;
        }
        let url = `api/claim?dateRange=${this.state.startDate.format(this.dateFormat)},${this.state.endDate.format(this.dateFormat)}`;
        axios
            .get(url)
            .then(function (response) {
                console.log(response.data);
                self.setState({
                    searchResults: response.data.result
                });
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    handleSearch(e) {
        e.preventDefault();
        const self = this;
        let url = `api/claim?dateRange=${this.state.startDate.format(this.dateFormat)},${this.state.endDate.format(this.dateFormat)}`;
        this.props.form.validateFields((err) => {
            if (!err) {
                axios
                    .get(url)
                    .then(function (response) {
                        console.log(response.data);
                        self.setState({
                            searchResults: response.data.result
                        });
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            } else {
                console.log("search form not submitted.");
            }
        });
    }

    onRangeChange(dates) {
        this.setState({
            startDate: dates[0],
            endDate: dates[1]
        });
    }

    onUnresolvedChange() {
        this.setState({
            showUnresolved: !this.state.showUnresolved
        });
    }

    onResolvedChange() {
        this.setState({
            showResolved: !this.state.showResolved
        });
    }

    isChecked(element, index, array) {
        const resolved = element.resolved;
        if (this.state.showUnresolved) {
            if (this.state.showResolved) {
                return true;
            } else {
                return !resolved;
            }
        } else {
            if (this.state.showResolved) {
                return resolved;
            } else {
                return false;
            }
        }
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <>
                <Badge showZero count={this.state.searchResults.filter((x) => x.resolved === false).length}></Badge>
                <Form onSubmit={this.handleSearch} layout="inline">
                    <Form.Item>
                        {getFieldDecorator('unresolved', {
                            valuePropName: 'checked',
                            initialValue: this.state.showUnresolved,
                        })(<Checkbox onChange={this.onUnresolvedChange}>Unresolved</Checkbox>)}
                    </Form.Item>
                    <Form.Item>
                        {getFieldDecorator('resolved', {
                            valuePropName: 'checked',
                            initialValue: this.state.showResolved,
                        })(<Checkbox onChange={this.onResolvedChange}>Resolved</Checkbox>)}
                    </Form.Item>
                    <Form.Item label="Range">
                        {getFieldDecorator("range", {
                            initialValue: [this.state.startDate, this.state.endDate],
                            rules: [
                                {
                                    required: true,
                                    message: "Please Select a date range."
                                }
                            ]
                        })(<RangePicker allowClear={false} format={this.dateFormat} onChange={this.onRangeChange} />)
                        }
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" htmlType="submit">
                            Search
                        </Button>
                    </Form.Item>
                </Form>
                <List
                    itemLayout="horizontal"
                    dataSource={this.state.searchResults.filter(this.isChecked)}
                    split={false}
                    renderItem={item => (
                        <List.Item>
                            <Ticket info={item} search={this.search}></Ticket>
                        </List.Item>
                    )}
                />,
            </>
        );
    }
}

export default Form.create()(TicketBox);