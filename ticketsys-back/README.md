```json
{
	"status" : <either "success" or "fail">,
    "result" : <the result>,
    "error"  :  {
    				"code" : <error code>
				}
}
```

### Error Response





### API

- `GET /api/v1/property` : List properties
- `GET /api/v1/property/:shortName` : Obtain a specific property indexed by shortname
- `POST /api/v1/property` : Create a new property
- `DELETE /api/v1/property/:shortName`



### Functionalities

1. Tenants file a claim

    - Get Today’s query number

    - Create a new claim based on provided info
    - Reply ticket number
    - Send email copy

2. Tenant Inquiry

    - Create a new inquery
    - Send email copy

3. Landlord

    - Obtain claim list (resolved, unresolved)

    - Mark claim resolved
    - Edit mailing list
    - Edit property information
