const express = require('express');
const path = require('path');
const models = require('./models.js');
const Op = require('sequelize').Op;
const Mustache = require('mustache');
// create express application
const app = express();
const port = 4000;
const mailConfig = require('./mailConfig.js');
const middlewares = require('./middlewares.js');
// for parsing application/json
app.use(express.json());
// static file frontend
app.use('/', express.static(path.join(__dirname, '../front/build')));
// static file uploaded
app.use('/static', express.static('static'));

// eslint-disable-next-line require-jsdoc
function constructResponse(status, result) {
  return {
    status: status,
    result: result,
  };
}

// ======================
// Property
/**
 * GET /api/property/:shortName
 * Obtain the detailed information of property referenced by the shortName
 *
 * --------
 * URL Parameters:
 * shortName: the shortName reference of the property
 */
app.get('/api/property/:shortName', (req, res) => {
  models.Property.findByPk(req.params.shortName)
      .then((property) => {
        if (property === null) {
          res.json(
              constructResponse('fail', `${req.params.shortName} 
              does not exist.`)
          );
        } else {
          res.json(constructResponse('success', property));
        }
      })
      .catch((err) => {
        res.json(constructResponse('fail', err));
      });
});

/**
 * GET /api/property
 * List all the properties
 *
 * --------
 * Query Parameters:
 * available: boolean value indicating the filtering on the
 *              availability of the the property
 * attributes: attributes should be included in the query,
 *              separated by comma (encoded)
 *              i.e. attributes=address_short%20price
 */
app.get('/api/property', function(req, res) {
  const where = {};
  let attributes = Object.keys(models.Property.rawAttributes);
  // conditions
  if (req.query.available === 'true') {
    where.available = true;
  } else if (req.query.available === 'false') {
    where.available = false;
  }
  if (req.query.attributes) {
    attributes = decodeURIComponent(req.query.attributes).split(',');
  }
  // query
  models.Property.findAll({
    where: where,
    attributes: attributes,
  })
      .then((properties) => {
        res.json(constructResponse('success', properties));
      })
      .catch((err) => {
        res.json(constructResponse('fail', err));
      });
});

// =======================================================
// Ticket
/**
 * POST /api/claim
 * Upload a claim
 *
 * --------
 * Body Parameters:
 * address_short: the shortName reference of the specific property
 *                      Could be obtained by `GET /api/property`
 * room_num: the supplementary room number used to identify
 *                   multiple tenants living in one property
 * phone_num: the phone number provided by tenant
 * email: the email provided by tenant
 * subject: the subject of the ticket
 * body: the detailed body of the ticket
 * name: the name of the tenant
 */
app.post('/api/claim',
    (req, res, next) => {
    // Calculating the primary key of the new ticket
    // Format: YYYYMMDD + 3 digit claim number
      const todayDate = new Date();
      todayDate.setHours(0, 0, 0, 0);

      const tomorrowDate = new Date();
      tomorrowDate.setDate(tomorrowDate.getDate() + 1);
      tomorrowDate.setHours(0, 0, 0, 0);

      const where = {
        createdAt: {
          [Op.gte]: todayDate,
          [Op.lt]: tomorrowDate,
        },
      };

      models.Ticket.count({where: where})
          .then((count) => {
            req.body.ticket_num =
          new Date()
              .toISOString()
              .substring(0, 10)
              .replace(/-/g, '') + ('000' + (count + 1)).slice(-3);

            return models.Ticket.create(req.body);
          })
          .then((ticket) => {
            res.json(constructResponse('success', ticket));
            next();
          })
          .catch((err) => {
            res.json(constructResponse('fail', err));
            next(err);
          });
    },
    (req, res, next) => {
      const rendered = Mustache.render(
          mailConfig.ClaimToTenantTemplate,
          Object.assign({}, mailConfig.templateMeta, req.body)
      );
      res.locals.mailComposer = {
        from: mailConfig.noreplyAddr,
        to: req.body.email,
        subject: `Maintenance Request ${req.body.ticket_num}`,
        html: rendered,
      };
      next();
    },
    middlewares.sendNoReply,
    (req, res, next) => {
      const rendered = Mustache.render(
          mailConfig.ClaimToMailingListTemplate,
          Object.assign({}, mailConfig.templateMeta, req.body)
      );

      models.MailingList.findAll({attributes: ['email']})
          .then((ml) => {
            return ml.map((m) => m.email).join(',');
          })
          .then((toGroup) => {
            res.locals.mailComposer = {
              from: mailConfig.noreplyAddr,
              to: toGroup,
              subject: `Maintenance Request ${req.body.ticket_num}`,
              html: rendered,
            };
            next();
          })
          .catch((err) => {
            then(err);
          });
    },
    middlewares.sendNoReply
);

// ==================================================================
// Inquiry
/**
 * POST /api/inquiry
 * Create a new inquiry
 * --------
 * Body Parameters:
 * address_short: the short name reference of the specific property
 *                      Could be obtained by `GET /api/property`
 * phone_num: the phone number provided by tenant
 * email: the email provided by tenant
 * body: the detailed body of the ticket
 * name: the name of the tenant
 */
app.post('/api/inquiry',
    (req, res, next) => {
      models.Inquiry.create(req.body,
          {
            raw: true,
          }
      )
          .then((inquiry) => {
            res.json(constructResponse('success', inquiry));
            inquiry = JSON.parse(JSON.stringify(inquiry));
            res.locals.inquiry = inquiry;
            console.log(inquiry);
            next();
          })
          .catch((err) => {
            res.json(constructResponse('fail', err));
            next(err);
          });
    },
    (req, res, next) => {
      const rendered = Mustache.render(
          mailConfig.InquiryToTenantTemplate,
          Object.assign({}, mailConfig.templateMeta, res.locals.inquiry)
      );
      res.locals.mailComposer = {
        from: mailConfig.noreplyAddr,
        to: res.locals.inquiry.email,
        subject: `Property Inquiry Submitted`,
        html: rendered,
      };
      next();
    },
    middlewares.sendNoReply
);

// ================================================================
// PropertyImage

/**
 * GET /api/property/:shortName/image
 * Obtain all image metadata associated with the address
 *  referenced by `shortName` (`address_short` of Property)
 */
app.get('/api/property/:shortName/image', (req, res) => {
  models.PropertyImage.findAll({
    where: {
      address_short: req.params.shortName,
    },
  })
      .then((imageMeta) => {
        res.json(constructResponse('success', imageMeta));
      })
      .catch((err) => {
        res.json(constructResponse('fail', err));
      });
});

// eslint-disable-next-line camelcase
const app_mgmt = require('./mgmt.js');
app.use('/management', app_mgmt);
app.listen(port, () => console.log(`Example app listening on port ${port}!`));
