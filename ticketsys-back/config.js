const fs = require('fs');

if (process.env.TICKETSYS_ENV == 'docker') {
  module.exports = {
    host: process.env.DATABASE_HOST || '127.0.0.1',
    database: 'root',
    user: 'TicketSys',
    password: 'qwer1234',
    port: 3306,
  };
} else {
  module.exports = JSON.parse(fs.readFileSync(__dirname + '/config.json'));
}