const fs = require('fs');
const or = require('get-orientation');

/**
 * Obtain the orientation of the given image specified by the path
 * @param {string} imageFilePath the path of the image
 * @return {number} a value that represents the image
 */
function getOrientation(imageFilePath) {
  const stream = fs.createReadStream(imageFilePath);
  return or.getOrientation(stream);
}

module.exports = {
  getOrientation: getOrientation,
};
