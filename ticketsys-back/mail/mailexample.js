const Mustache = require('mustache');
const meta = require('../mailConfig.js').templateMeta;

const dummyData = {
  ticket_num: 20190908110,
  name: 'Tan Li',
  address_short: 'Address 2',
  phone_num: '216-999-8888',
  email: 'txl666@case.edu',
  subject: 'Window fell off',
  body: 'The window on the second floor has fell off.\n' +
            'It is freezing in the room. Please fix this.\n' +
            'Thanks',
};

const views = Object.assign({}, meta, dummyData);
const fs = require('fs');
const template =
    fs.readFileSync('./resources/ClaimToTenantTemplate.html').toString();
const rendered = Mustache.render(template, views);
fs.writeFileSync('./resources/rendered.html', rendered);
