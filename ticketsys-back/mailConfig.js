const fs = require('fs');

/**
 * Construct a html href tag
 * @param {string} link the hyperlink
 * @param {string} reference the reference of the link
 * @return {string} constructed html href tag
 */
function makeHref(link, reference) {
  if (reference === undefined || reference === null) {
    reference = link;
  }
  return `<a href=${link}>${reference}</a>`;
}

/**
 * Construct a html href mailto tag
 * @param {string} emailAddress
 * @param {string} reference
 * @return {string} constructed html return tag
 */
function makeMailto(emailAddress, reference) {
  if (reference === undefined || reference === null) {
    reference = emailAddress;
  }
  return makeHref(
      `mailto:${emailAddress}`,
      reference
  );
}

const meta = {
  contact_person: 'Jon Schaefer',
  contact_phone: '(216) 509-6361',
  contact_email: () => {
    return makeMailto('jjfschaefer@aol.com');
  },
  company_name: 'Circle North Properties',
  company_website: () => {
    return makeHref('www.circlenorthproperties.com');
  },
  company_address: '10900 Euclid Ave, Cleveland, OH 44106',
};

const ClaimToTenantTemplate =
  fs.readFileSync('./mail/ClaimToTenantTemplate.html').toString();

const ClaimToMailingListTemplate =
  fs.readFileSync('./mail/ClaimToMailingListTemplate.html').toString();

const InquiryToTenantTemplate = 
  fs.readFileSync('./mail/InquiryToTenantTemplate.html').toString();

const InquiryToLandlordTemplate = 
  fs.readFileSync('./mail/InquiryToLandlordTemplate.html').toString();


module.exports.templateMeta = meta;
module.exports.noreplyAddr = 'noreply@circlenorthproperties.com';
module.exports.ClaimToTenantTemplate = ClaimToTenantTemplate;
module.exports.ClaimToMailingListTemplate = ClaimToMailingListTemplate;
module.exports.InquiryToLandlordTemplate = InquiryToLandlordTemplate;
module.exports.InquiryToTenantTemplate = InquiryToTenantTemplate;
