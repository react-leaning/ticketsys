const express = require('express');
const path = require('path');
const models = require('./models.js');
const Op = require('sequelize').Op;
const multer = require('multer');
const short = require('short-uuid');
const getOrientation = require('./image.js').getOrientation;
const basicAuth = require('express-basic-auth');

// express app
const mgmtpw = require('./password.js');
const appManagement = express();
appManagement.use(basicAuth({
  users: mgmtpw.password,
  challenge: true,
  realm: 'CiRcLe709394',
}));
// handling JSON
appManagement.use(express.json());
// static file frontend
appManagement.use(
    '/',
    express.static(path.join(__dirname, '../management/build'))
);
// static file upload
appManagement.use('/static', express.static('static'));

// eslint-disable-next-line require-jsdoc
function constructResponse(status, result) {
  return {
    status: status,
    result: result,
  };
}

//  ===================================================================
//  Property
/**
 * POST /api/property/:shortName
 * Performs partial update on property referenced by the shortName
 *
 * --------
 * URL Parameters:
 * shortName: the shortName reference of the property
 *
 * --------
 * Body Parameters:
 * available : boolean value indicating the availability of the property
 * availability_info : availability information of the property
 * info: the listing information
 * feature: the feature introduction of the house
 * price: the monthly price
 */
appManagement.post('/api/property/:shortName', (req, res) => {
  models.Property.update(req.body, {
    where: {
      address_short: req.params.shortName,
    },
  }).then(
      (result) => {
        if (!result[0]) {
          res.json(
              constructResponse(
                  'fail',
                  `${req.params.shortName} does not exist`)
          );
          return;
        }
        res.json(constructResponse('success', true));
      },
      (err) => {
        res.json('fail', err);
      }
  );
});

/**
 * POST /api/property
 * Create a new property
 * --------
 * Body Parameters:
 * address_short: the short name of the property
 * address_long: the full address of the property
 * price: integer value indicating the monthly price of the property
 * info: supplemental information
 * feature: feature of the property
 * available: boolean value indicating the availability of the property
 * availability_info : availability information of this property
 * image_location : the location of the housing images
 */
appManagement.post('/api/property', (req, res) => {
  models.Property.create(req.body)
      .then((property) => {
        res.json(constructResponse('success', property));
      })
      .catch((err) => {
        res.json(constructResponse('fail', err));
      });
});

/**
 * GET /api/property
 * List all the properties
 *
 * --------
 * Query Parameters:
 * available: boolean value indicating the filtering on the
 *              availability of the the property
 * attributes: attributes should be included in the query,
 *              separated by comma (encoded)
 *              i.e. attributes=address_short%20price
 */
appManagement.get('/api/property', (req, res) => {
  const where = {};
  let attributes = Object.keys(models.Property.rawAttributes);
  // conditions
  if (req.query.available === 'true') {
    where.available = true;
  } else if (req.query.available === 'false') {
    where.available = false;
  }
  if (req.query.attributes) {
    attributes = decodeURIComponent(req.query.attributes).split(',');
  }
  // query
  models.Property.findAll({
    where: where,
    attributes: attributes,
  })
      .then((properties) => {
        res.json(constructResponse('success', properties));
      })
      .catch((err) => {
        res.json(constructResponse('fail', err));
      });
});

// ===================================================================
// ticket
/**
 * GET /api/claim/:ticketNum
 * Obtain the detailed information of ticket referenced by the ticketNumber
 *
 * --------
 * URL Parameters:
 * ticketNum: the ticketNum reference of the ticket
 */
appManagement.get('/api/claim/:ticketNum', (req, res) => {
  models.Ticket.findByPk(req.params.ticketNum)
      .then((ticket) => {
        if (ticket == null) {
          res.json(
              constructResponse(
                  'fail',
                  `${req.params.ticketNum} does not exist.`)
          );
        } else {
          res.json(constructResponse('success', ticket));
        }
      })
      .catch((err) => {
        res.json(constructResponse('fail', err));
      });
});

/**
 * POST /api/claim/:ticketNum
 * Performs partial update on ticket referenced by the ticketNum
 * Specifically, resolved
 *
 * --------
 * URL Parameters:
 * ticketNum: the ticketNum reference of the ticket
 *
 * --------
 * Body Parameters:
 * resolved : boolean value indicating the ticket is resolved
 */
appManagement.post('/api/claim/:ticketNum', (req, res) => {
  const body = {};
  for (const key in req.body) {
    if (key != 'resolved') {
      body[key] = req.body[key];
    }
  }
  if (
    (res.body && res.body.resolved === 'undefined') ||
        Object.keys(body).length != 0
  ) {
    res.json(constructResponse('fail', 'Only `resolved` can be updated'));
  }

  models.Ticket.update(req.body, {
    where: {
      ticket_num: req.params.ticketNum,
    },
  }).then(
      (result) => {
        if (!result[0]) {
          res.json(
              constructResponse(
                  'fail',
                  `${req.params.ticketNum} does not exist`)
          );
          return;
        }
        res.json(constructResponse('success', true));
      },
      (err) => {
        res.json('fail', err);
      }
  );
});

/**
 * GET /api/claim
 * List all claims
 *
 * --------
 * Query Parameters:
 * resolved: boolean value indicating whether the ticket is
 *              resolved or not
 * attributes: attributes should be included in the query,
 *              separated by comma (encoded)
 *              i.e. attributes=address_short%20price
 * dateRange: the date range for filtering all claims,
 *              separated by comma (encoded)
 *              each date should satisfy ISO format of YYYY/MM/DD (encoded)
 *              i.e. `dateRange=2019%2F05%2F04%2C2019%2F05%2F05` filters
 *                  inquiries whose 2019/05/04 <= createdAt < 2019/05/05
 */
appManagement.get('/api/claim', (req, res) => {
  const where = {};
  let attributes = Object.keys(models.Ticket.rawAttributes);

  // conditions
  if (req.query.resolved === 'true') {
    where.resolved = true;
  } else if (req.query.resolved === 'false') {
    where.resolved = false;
  }

  if (req.query.dateRange) {
    dateRange = decodeURIComponent(req.query.dateRange).split(',');
    if (dateRange.length == 2) {
      dategte = new Date(dateRange[0]);
      datelt = new Date(dateRange[1]);
      if (!isNaN(dategte) && !isNaN(datelt) && dategte < datelt) {
        where.createdAt = {
          [Op.gte]: dategte,
          [Op.lt]: datelt,
        };
      }
    }
  }

  if (req.query.attributes) {
    attributes = decodeURIComponent(req.query.attributes).split(',');
  }

  models.Ticket.findAll({
    where: where,
    attributes: attributes,
  })
      .then((tickets) => {
        res.json(constructResponse('success', tickets));
      })
      .catch((err) => {
        res.json(constructResponse('fail', err));
      });
});

// ==================================================================
// Inquiry
/**
 * GET /api/inquiry/:id
 * Obtain the detailed information of inquiry indexed by id
 *
 * --------
 * URL Parameters:
 * id: the primary key reference of the inquiry
 */
appManagement.get('/api/inquiry/:id', (req, res) => {
  models.Inquiry.findByPk(req.params.id)
      .then((inquiry) => {
        if (inquiry === null) {
          res.json(constructResponse(
              'fail',
              `${req.params.id} does not exist.`));
        } else {
          res.json(constructResponse('success', inquiry));
        }
      })
      .catch((err) => {
        res.json(constructResponse('fail', err));
      });
});

/**
 * POST /api/inquiry/:id
 * Performs partial update on inquiry referenced by the id
 * Specifically, resolved
 *
 * --------
 * URL Parameters:
 * id: the primary key reference of the ticket
 *
 * --------
 * Body Parameters:
 * resolved : boolean value indicating the ticket is resolved
 */
appManagement.post('/api/inquiry/:id', (req, res) => {
  const body = {};
  for (const key in req.body) {
    if (key != 'resolved') {
      body[key] = req.body[key];
    }
  }
  delete body.resolved;
  if (
    (res.body && res.body.resolved === 'undefined') ||
        Object.keys(body).length != 0
  ) {
    res.json(constructResponse('fail', 'Only `resolved` can be updated'));
  }
  //   models.Inquiry.findByPk(req.params.id)
  //     .then(inquiry => {
  //       if (inquiry === null) {
  //         res.json(constructResponse("fail", `${req.params.id} does not exist.`));
  //         return;
  //       }
  //       inquiry
  //         .update(req.body)
  //         .then(inquiry => {
  //           res.json(constructResponse("success", inquiry));
  //         })
  //         .catch(err => {
  //           res.json(constructResponse("fail", err));
  //         });
  //     })
  //     .catch(err => {
  //       res.json(constructResponse("fail", err));
  //     });
  models.Inquiry.update(req.body, {
    where: {
      id: req.params.id,
    },
  }).then(
      (result) => {
        if (!result[0]) {
          res.json(constructResponse(
              'fail',
              `${req.params.id} does not exist`));
          return;
        }
        res.json(constructResponse('success', true));
      },
      (err) => {
        res.json('fail', err);
      }
  );
});

/**
 * GET /api/inquiry
 * List all inquiries
 *
 * --------
 * Query Parameters:
 * resolved: boolean value indicating the inquiry is resolved
 * attributes: attributes should be included in the query,
 *              separated by comma (encoded)
 *              i.e. `attributes=address_short%20price`
 *                  selects only `address_short` and `price`
 *                  attributes in the result set
 * dateRange: the date range for filtering all inquiries,
 *              separated by comma (encoded)
 *              each date should satisfy ISO format of YYYY/MM/DD (encoded)
 *              i.e. `dateRange=2019%2F05%2F04%2C2019%2F05%2F05` filters
 *                  inquiries whose 2019/05/04 <= createdAt < 2019/05/05
 */
appManagement.get('/api/inquiry', (req, res) => {
  const where = {};
  let attributes = Object.keys(models.Inquiry.rawAttributes);

  // conditions
  if (req.query.resolved === 'true') {
    where.resolved = true;
  } else if (req.query.resolved === 'false') {
    where.resolved = false;
  }

  if (req.query.dateRange) {
    dateRange = decodeURIComponent(req.query.dateRange).split(',');
    if (dateRange.length == 2) {
      dategte = new Date(dateRange[0]);
      datelt = new Date(dateRange[1]);
      if (!isNaN(dategte) && !isNaN(datelt) && dategte < datelt) {
        where.createdAt = {
          [Op.gte]: dategte,
          [Op.lt]: datelt,
        };
      }
    }
  }

  if (req.query.attributes) {
    attributes = decodeURIComponent(req.query.attributes).split(',');
  }

  models.Inquiry.findAll({
    where: where,
    attributes: attributes,
  })
      .then((tickets) => {
        res.json(constructResponse('success', tickets));
      })
      .catch((err) => {
        res.json(constructResponse('fail', err));
      });
});

// ===========================================================================
// MailingList
// /**
//  * DELETE /api/mailinglist/:id
//  * Delete the specific email address in the mailing list
//  *
//  * --------
//  * URL Parameters:
//  * id: the primary key reference of the email address
//  */
// appManagement.delete('/api/mailinglist/:id', (req, res) => {
//   models.MailingList.findByPk(req.params.id)
//       .then((email) => {
//         if (email === null) {
//           res.json(constructResponse(
//               'fail',
//               `${req.params.id} does not exist.`));
//         } else {
//           res.json(constructResponse('success', email));
//         }
//       })
//       .catch((err) => {
//         res.json(constructResponse('fail', err));
//       });
// });

// /**
//  * POST /api/mailinglist
//  * Create a new mailinglist entry
//  * --------
//  * Body Parameters:
//  * name: the name of the address owner
//  * email: the email address
//  */
// appManagement.post('/api/mailinglist', (req, res) => {
//   models.MailingList.create(req.body)
//       .then((mail) => {
//         res.json(constructResponse('success', mail));
//       })
//       .catch((err) => {
//         res.json(constructResponse('fail', err));
//       });
// });

/**
 * POST /api/mailinglist
 * Updates all mailing list
 */
appManagement.post('/api/mailinglist', (req, res) => {
  models.MailingList.destroy({
    truncate: true,
  })
      .then((_) => {
        return models.MailingList.bulkCreate(
            req.body,
        );
      })
      .then((createResult) => {
        res.json(constructResponse('success', createResult));
      })
      .catch((err) => {
        res.json(constructResponse('fail', {
          reason: 'unable to create; check the syntax',
          body: res.body,
        }));
      });
});

/**
 * GET /api/mailinglist
 * List all mailing list
 *
 * --------
 * Query Parameters:
 * attributes: attributes should be included in the query,
 *              separated by comma (encoded)
 *              i.e. attributes=email
 */
appManagement.get('/api/mailinglist', (req, res) => {
  const where = {};
  let attributes = Object.keys(models.MailingList.rawAttributes);

  // conditions
  if (req.query.attributes) {
    attributes = decodeURIComponent(req.query.attributes).split(',');
  }

  models.MailingList.findAll({
    where: where,
    attributes: attributes,
  })
      .then((tickets) => {
        res.json(constructResponse('success', tickets));
      })
      .catch((err) => {
        res.json(constructResponse('fail', err));
      });
});

// ==============================================================
// Property image upload

// The image upload middleware that handles multipart form
// submission of uploads
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'static/');
  },
  // automatically renames the uploaded file
  filename: (req, file, cb) => {
    cb(null, newRandomFileName(req, file));
  },
});

const upload = multer({
  storage: storage,
});

/**
 * generates random file name for file uploads in the form
 * `OriginalFileName-{short UUID}`
 * @param {express.request} req the express request
 * @param {multer.file}  file the multer file instance of the upload
 * @return {string} a string that represents the random filename with extension
 */
function newRandomFileName(req, file) {
  const rfn =
        new Date()
            .toISOString()
            .substring(0, 10)
            .replace(/-/g, '') +
        '-' +
        short.generate();
  let ext = path.parse(file.originalname).ext;
  if (ext == '') {
    ext = req.file.mimetype.split('/')[1];
  }
  return rfn + ext;
}

/**
 * Return the static file host location
 * Used for scalability
 * @param {express.request} req the express request
 * @return {string} the complete url for requesting static file
 */
function getImageLocation(req) {
  return `${req.protocol}://${req.get('host')}/static`;
}

/**
 * GET /api/property/image
 * Obtain all image metadata of the property information
 */
appManagement.get('/api/propertyImage', (req, res) => {
  models.PropertyImage.findAll()
      .then((imgmeta) => {
        res.json(constructResponse('success', imgmeta));
      })
      .catch((err) => {
        res.json(constructResponse('fail', err));
      });
});

/**
 * GET /api/property/:shortName/image
 * Obtain all image metadata associated with the address
 *  referenced by `shortName` (`address_short` of Property)
 */
appManagement.get('/api/property/:shortName/image', (req, res) => {
  models.PropertyImage.findAll({
    where: {
      address_short: req.params.shortName,
    },
  })
      .then((imageMeta) => {
        res.json(constructResponse('success', imageMeta));
      })
      .catch((err) => {
        res.json(constructResponse('fail', err));
      });
});

// supported MIME form submission image types
const SUPPORTED_MIME_IMAGE_TYPE = ['gif', 'jpeg', 'png', 'bmp'];

/**
 * POST /api/property/:shortName/image/upload
 * Upload an image file and associate this image with the property
 *  identified by `shortName` (the `address_short` property)
 *  The name of the image will be used as the identifier
 *  Returns an json response including the metadata created in the
 *  database
 */
appManagement.post(
    '/api/property/:shortName/image/upload',
    upload.single('img'),
    (req, res) => {
      if (req.file.mimetype === undefined) {
        res.json(
            constructResponse('fail', {
              reason: 'no file is uploaded',
            })
        );
      }
      const filetype = req.file.mimetype.split('/');
      if (filetype[0] != 'image' || !filetype[1] in SUPPORTED_MIME_IMAGE_TYPE) {
        res.json(
            constructResponse('fail', {
              reason: 'Unsupported image type',
              fileMIMEType: req.file.mimetype,
              supportedType: SUPPORTED_MIME_IMAGE_TYPE,
            })
        );
        return;
      }
      const originalFileName = path.parse(req.file.originalname).name;
      if (originalFileName == '') {
        res.json(
            constructResponse('fail', {
              reason: 'Invalid file name.',
            })
        );
      }

      getOrientation(req.file.path)
          .then((orientation) => {
            const attrs = {
              address_short: req.params.shortName,
              name: originalFileName,
              file_path: getImageLocation(req) + '/' + req.file.filename,
              orientation: orientation,
            };

            models.PropertyImage.create(attrs)
                .then((metadata) => {
                  res.json(constructResponse('success', metadata));
                })
                .catch((err) => {
                  res.json(constructResponse('fail', err));
                });
          });
    }
);

/**
 * POST /api/changeUsernamePassword
 * change password and/or username
 *
 * Body Parameters
 * ---------------
 * username : the new username; if empty, the username will remain the same
 * password : the new password; can be empty
 */
appManagement.post('/api/changeUsernamePassword', (req, res) => {
  try {
    mgmtpw.changeUsernamePassword(req.body.username, req.body.password);
  } catch (err) {
    res.json(constructResponse(
        'fail',
        {reason: 'unable to change password', err: err}
    ));
  }

  res.json(constructResponse('success', {}));
});

module.exports = appManagement;
