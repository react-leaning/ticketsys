const sendmail = require('sendmail')();

/**
 * send email based on context provided
 * @param {express.request} req http request context
 * @param {express.response} res http response context
 * @param {callback} next callback to next middleware
 */
function sendNoReply(req, res, next) {
  sendmail(res.locals.mailComposer, (err, reply) => {
    console.log(err);
    console.log(reply);
    if (err) {
      next(err);
    } else {
      next();
    }
  });
}


module.exports.sendNoReply = sendNoReply;
