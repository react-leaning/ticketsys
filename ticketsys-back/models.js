const Sequelize = require('sequelize');

// read the configuration from file
// `dbConfig.json` stores the information needed to connect to the mysql database
const dbConfig = require('./config.js');
dbConfig.dialect = 'mysql';
dbConfig.dialectOptions = {
  supportBigNumbers: true,
};
dbConfig.pool = {
  max: 5,
  min: 0,
  acquire: 30000,
  idle: 10000,
};
const sequelize = new Sequelize(dbConfig);

const Model = Sequelize.Model;

class Property extends Model {}
Property.init(
    {
      address_short: {
        type: Sequelize.STRING(60),
        allowNull: false,
        primaryKey: true,
      },
      address_long: {
        type: Sequelize.STRING(300),
        allowNull: false,
      },
      price: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      info: {
        type: Sequelize.STRING(500),
      },
      feature: {
        type: Sequelize.STRING(500),
      },
      available: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true,
      },
      availability_detail: {
        type: Sequelize.STRING(500),
      },
    },
    {
      sequelize,
    }
);

class Ticket extends Model {}
Ticket.init(
    {
      ticket_num: {
        type: Sequelize.STRING(13),
        allowNull: false,
        primaryKey: true,
      },
      address_short: {
        type: Sequelize.STRING(60),
        allowNull: false,
      },
      room_num: {
        type: Sequelize.STRING(20),
        defaultValue: '',
      },
      phone_num: {
        type: Sequelize.STRING(20),
        allowNull: false,
      },
      email: {
        type: Sequelize.STRING(100),
        allowNull: false,
      },
      subject: {
        type: Sequelize.STRING(100),
        allowNull: false,
      },
      body: {
        type: Sequelize.STRING(800),
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING(50),
        allowNull: false,
      },
      resolved: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
    },
    {
      sequelize,
    }
);

class Inquiry extends Model {}
Inquiry.init(
    {
      address_short: {
        type: Sequelize.STRING(60),
        allowNull: false,
      },
      phone_num: {
        type: Sequelize.STRING(20),
        allowNull: false,
      },
      email: {
        type: Sequelize.STRING(100),
        allowNull: false,
      },
      body: {
        type: Sequelize.STRING(800),
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING(50),
        allowNull: false,
      },
      resolved: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
    },
    {
      sequelize,
    }
);

class MailingList extends Model {}
MailingList.init(
    {
      name: {
        type: Sequelize.STRING(50),
        allowNull: false,
      },
      email: {
        type: Sequelize.STRING(100),
        primaryKey: true,
        allowNull: false,
      },
    },
    {
      sequelize,
    }
);

class PropertyImage extends Model {}
PropertyImage.init(
    {
      address_short: {
        type: Sequelize.STRING(60),
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING(50),
      },
      file_path: {
        type: Sequelize.STRING(400),
        allowNull: false,
      },
      orientation: {
        type: Sequelize.SMALLINT,
        defaultValue: 0,
        allowNull: false,
      },
    },
    {
      sequelize,
    }
);

sequelize
    .authenticate()
    .then(() => {
      console.log('Connection has been established successfully.');
    })
    .catch((err) => {
      console.error('Unable to connect to the database:', err);
    });

// sequelize.sync({alter: true});

// export
module.exports = {
  sequelize: sequelize,
  Property: Property,
  Ticket: Ticket,
  Inquiry: Inquiry,
  MailingList: MailingList,
  PropertyImage: PropertyImage,
};
