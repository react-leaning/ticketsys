const fs = require('fs');

authinfo = JSON.parse(fs.readFileSync('./password.json').toString());
password = {};
password[authinfo.username] = authinfo.password;

/**
 * update the username and password
 * @param {*} username new username
 * @param {*} password new password
 */
function changeUsernamePassword(username, password) {
  const authinfo = JSON.parse(fs.readFileSync('./password.json').toString());
  if (username !== null && username !== undefined && username !== '') {
    authinfo['username'] = username;
  }
  authinfo['password'] = password;

  fs.writeFileSync('./password.json', JSON.stringify(authinfo));
  this.password = {};
  this.password[authinfo.username] = authinfo.password;
}

module.exports = {
  changeUsernamePassword: changeUsernamePassword,
  password: password,
};
